
-- 类 = class(类名,[父类])
local MainScene = class("MainScene", function()
    return display.newScene("MainScene")
end)

local GRAVITY         = -1000
local COIN_MASS       = 100
local COIN_RADIUS     = 46
local COIN_FRICTION   = 0.8
local COIN_ELASTICITY = 0.8
local WALL_THICKNESS  = 64
local WALL_FRICTION   = 1.0
local WALL_ELASTICITY = 0.5
local ENEMY_MASS      = 20

-- 碰撞类型 setCollisionType
local TYPE_BOTTOM     = 1
local TYPE_ENEMY      = 2
local TYPE_HERO		  = 3

-- 包子形状
local points_set = {{-29.5,-23}, {-34.5,-15},{-32,1},{-18,17.5},{0,23},{18,17.5},{32,1},{34.5,-15},{29.5,-23}}


-- ctor() 在MainScene.new() 执行的时候会自动调用
function MainScene:ctor()
    -- 创建图层, 场景可以直接添加精灵, 图层是提供触摸时间功能
    self.layer = display.newLayer()
    self.layer:addTouchEventListener(function(event, x,y)
        return self:onTouch(event, x,y)
    end, false) -- 第二个参数:是否多点触摸
    self:addChild(self.layer)
    -- 设置图层触摸使能
    self.layer:setTouchEnabled(true)

    -- 创建物理世界
    self.world = CCPhysicsWorld:create(0, GRAVITY)
    -- 添加物理世界为场景的子节点
    self:addChild(self.world)

	-- 创建墙
	self:createWall()
	-- 杆子
    self:createGanzi(600):setPosition(display.cx,display.cy-80)
    self:createGanzi(300):setPosition(150, display.cy+100)
	self:createGanzi(300):setPosition(display.width-150, display.cy+100)
	--创建主角
    self.hero = self:createHero(display.cx,100)

	-- 创建敌人
    self.batch_enemy = display.newBatchNode("baozi2.png")
    self:addChild(self.batch_enemy)
	self:createEnemy(100,500)
	self:createEnemy(200,500)
	self:createEnemy(500,400)
	self:createEnemy(700,500)

	-- 敌人包子和地板的碰撞检测
    self.world:addCollisionScriptListener(
    function(strtype,event)
    	if strtype=="begin" then
			local enemy_body = event:getBody2()
			local body = event:getBody2()
			local node = body:getNode()
			local x,y = node:getPosition()
			body:unbind()
			self.batch_enemy:removeChild(node)
			body:removeSelf()
			local fenmu = display.newSprite("fenmu.png",x,y)
			self:addChild(fenmu)
			transition.execute(fenmu,CCFadeOut:create(0.5),{onComplete = function()
					fenmu:getParent():removeChild(fenmu)
	        	end})
		end
    end,
    TYPE_BOTTOM, TYPE_ENEMY)

	-- 敌人和主角的碰撞检测
	self.isCollision = false
     self.world:addCollisionScriptListener(
    function(strtype,event)
    	-- 如果在检测回调中,则返回
		if self.isCollision then return true end

    	if strtype=="begin" then
    		self.isCollision = true;
			local enemy_body = event:getBody2()
			local body = event:getBody2()
			local node = body:getNode()
			--if node == nil then return true end
			local x,y = node:getPosition()
			node:setVisible(false)
			body:unbind()
			local hurtSprite = display.newSprite("hurt.png",x,y)
			self:addChild(hurtSprite)
			body:bind(hurtSprite)
			transition.execute(hurtSprite,CCDelayTime:create(0.1),{onComplete = function()
					body:unbind()
					hurtSprite:getParent():removeChild(hurtSprite)
					node:setVisible(true)
					body:bind(node)
					self.isCollision = false;
	        	end})
		end
		return true  -- 调用原来的碰撞效果
    end,
    TYPE_HERO, TYPE_ENEMY)

    -- 调试物理形状
    --self.worldDebug = self.world:createDebugNode()
    --self:addChild(self.worldDebug)

	-- 开始
     self.world:start()
end

-- 创建墙
function MainScene:createWall()
	-- add static body
    local leftWallSprite = display.newSprite("Wall.png")
    leftWallSprite:setScaleY(display.height / WALL_THICKNESS)
    leftWallSprite:setScaleX(10 / WALL_THICKNESS)
    -- self.batch:addChild(leftWallSprite)
    self:addChild(leftWallSprite)
    -- CCPhysicsBody *createBoxBody(float mass, float width, float height);
    local leftWallBody = self.world:createBoxBody(0, 10, display.height)
    leftWallBody:setFriction(WALL_FRICTION) -- 摩擦力
    leftWallBody:setElasticity(WALL_ELASTICITY) -- 弹力
    -- 物理绑定图片
    leftWallBody:bind(leftWallSprite)
    leftWallBody:setPosition(display.left + 10 / 2, display.cy)

    local rightWallSprite = display.newSprite("Wall.png")
    rightWallSprite:setScaleY(display.height / WALL_THICKNESS)
    rightWallSprite:setScaleX(10 / WALL_THICKNESS)
    -- self.batch:addChild(rightWallSprite)
    self:addChild(rightWallSprite)
    local rightWallBody = self.world:createBoxBody(0, 10, display.height)
    rightWallBody:setFriction(WALL_FRICTION)
    rightWallBody:setElasticity(WALL_ELASTICITY)
    rightWallBody:bind(rightWallSprite)
    rightWallBody:setPosition(display.right - 10 / 2, display.cy)

    local bottomWallSprite = display.newSprite("Wall.png")
    bottomWallSprite:setScaleX(display.width / WALL_THICKNESS)
    bottomWallSprite:setScaleY(10 / WALL_THICKNESS)
    -- self.batch:addChild(bottomWallSprite)
    self:addChild(bottomWallSprite)
    local bottomWallBody = self.world:createBoxBody(0, display.width, 10)
    bottomWallBody:setFriction(WALL_FRICTION)
    bottomWallBody:setElasticity(WALL_ELASTICITY)
    bottomWallBody:bind(bottomWallSprite)
    bottomWallBody:setPosition(display.cx, display.bottom + 10 / 2)
    bottomWallBody:setCollisionType(TYPE_BOTTOM)

     local topWallSprite = display.newSprite("Wall.png")
    topWallSprite:setScaleX(display.width / WALL_THICKNESS)
    topWallSprite:setScaleY(10 / WALL_THICKNESS)
    -- self.batch:addChild(bottomWallSprite)
    self:addChild(topWallSprite)
    local topWallBody = self.world:createBoxBody(0, display.width, 10)
    topWallBody:setFriction(WALL_FRICTION)
    topWallBody:setElasticity(WALL_ELASTICITY)
    topWallBody:bind(topWallSprite)
    topWallBody:setPosition(display.cx, display.top-10/2)
end

-- 杆子
function MainScene:createGanzi(length)
	-- 3根杆子
   	local ganziSprite = display.newSprite("Wall.png")
	ganziSprite:setScaleX(length/(ganziSprite:getContentSize().height))
	ganziSprite:setScaleY(0.5)
	self:addChild(ganziSprite)
	local size = ganziSprite:getContentSize()

	local scaleX = ganziSprite:getScaleX()
	local scaleY = ganziSprite:getScaleY()

	-- echoInfo(size.width*scaleX)
	local ganziBody = self.world:createBoxBody(0,size.width*scaleX,size.height*scaleY)
	ganziBody:setFriction(WALL_FRICTION)
	ganziBody:setElasticity(WALL_ELASTICITY)
	ganziBody:bind(ganziSprite)

	return ganziBody
end

 -- 创建主角
function MainScene:createHero(x, y)
    -- add sprite to scene
    --local coinSprite = CCSprite:createWithTexture(self.batch:getTexture())
	local heroSprite = display.newSprite("baozi1.png")
    --self.batch:addChild(heroSprite)
	self:addChild(heroSprite)

    -- create body
    local points = CCPointArray:create(10)

	for k,v in pairs(points_set) do
		--echoInfo(v[1] .. " " .. v[2])
		points:add(CCPoint(v[1],v[2]))
	end

    local heroBody = self.world:createPolygonBody(COIN_MASS, points)
    heroBody:setFriction(COIN_FRICTION)
    heroBody:setElasticity(COIN_ELASTICITY)
    -- binding sprite to body
    heroBody:bind(heroSprite)
    -- 碰撞类型
    heroBody:setCollisionType(TYPE_HERO)
    -- set body position
    heroBody:setPosition(x, y)
    return heroBody
end

-- 创建敌人
function MainScene:createEnemy(x,y)
	 local enemySprite = CCSprite:createWithTexture(self.batch_enemy:getTexture())
    self.batch_enemy:addChild(enemySprite)

    -- create body
    local points = CCPointArray:create(10)

	for k,v in pairs(points_set) do
		points:add(CCPoint(v[1],v[2]))
	end

    local enemyBody = self.world:createPolygonBody(ENEMY_MASS, points)
    enemyBody:setFriction(COIN_FRICTION)
    enemyBody:setElasticity(COIN_ELASTICITY)
    -- binding sprite to body
    enemyBody:bind(enemySprite)
    -- set body position
    enemyBody:setPosition(x, y)
    enemyBody:setCollisionType(TYPE_ENEMY)
    return enemyBody
end

-- 触摸响应
function MainScene:onTouch(event, x,y)
    if event == "began" then
    	local bx,by = self.hero:getPosition()
    	local sx,sy = x-bx,y-by
    	local speedmin = 700
		if math.abs(sx)>math.abs(sy) then
			if math.abs(sx) < speedmin then
				sy=math.abs(speedmin/sx)*sy
				sx=speedmin*sx/math.abs(sx)
			end
		else
			if math.abs(sy) < speedmin then
				sx=math.abs(speedmin/sy)*sx
				sy=speedmin*sy/math.abs(sy)
			end
		end

		self.hero:setVelocity(sx, sy)
    elseif event == "moved" then
	elseif event == "ended" then
	end

	return true --这里如果不返回true, 就会收不到 moved ende 触摸事件
end


return MainScene
