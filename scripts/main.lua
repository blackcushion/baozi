
--错误回调函数
function __G__TRACKBACK__(errorMessage)
    CCLuaLog("----------------------------------------")
    CCLuaLog("LUA ERROR: " .. tostring(errorMessage) .. "\n")
    --可以在任何时候调用debug.traceback获取当前运行的traceback信息
    CCLuaLog(debug.traceback("", 2))
    CCLuaLog("----------------------------------------")
end

--quick 框架, 不加这个压缩包也可以导入框架, 但是要把框架文件夹拷贝到脚本文件夹下
CCLuaLoadChunksFromZip("res/framework_precompiled.zip")

xpcall(function()
    require("game")
    game.startup()
end, __G__TRACKBACK__)
