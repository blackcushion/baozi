
require("config")
require("framework.init")
--framework是个文件夹, 导入framework下的init.lua

-- define global module
game = {}

--main调用 game.startup
function game.startup()
    CCFileUtils:sharedFileUtils():addSearchPath("res/")
    --这个两个常量在config.lua中定义
    --display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME, GAME_TEXTURE_IMAGE_FILENAME)

	--去掉FPS
	CCDirector:sharedDirector():setDisplayStats(false)

    game.enterMainScene()
end

function game.exit()
    CCDirector:sharedDirector():endToLua()
end

function game.enterMainScene()
    display.replaceScene(require("scenes.MainScene").new(), "fade", 0.6, display.COLOR_WHITE)
end
